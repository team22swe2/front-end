import React from "react";

class FetchHeartBeat extends React.Component {
  state = {
    loading: true,
    heartbeat: null
  };

  async componentDidMount() {
    const url = "https://team22.softwareengineeringii.com/cic/heartbeat";
    const response = await fetch(url);
    const data = await response.json();
    const latestHeartbeat = data[0];
    this.setState({ heartbeat: latestHeartbeat._id, loading: false });
  }

  render() {
    if (this.state.loading) {
      return <div>loading...</div>;
    }

    if (!this.state.heartbeat) {
      return <div>didn't get heartbeat</div>;
    }
    return (
      <div>
        <div>Latest heartbeat id: {this.state.heartbeat}</div>
      </div>
    );
  }
}
export default FetchHeartBeat;
