import React from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Home } from './Home';
import { ViewGateway } from './ViewGateway';
import { RunDiagnostics } from './RunDiagnostics';
import { NoMatch } from './NoMatch';
import { Layout } from './components/Layout';
import { NavigationBar } from './components/NavigationBar';
import { Jumbotron } from './components/Jumbotron'; 

function App() {
  return (
    <React.Fragment>
      <Router>
      <NavigationBar/>
      <Jumbotron />
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/ViewGateway" component={ViewGateway} />
          <Route path="/RunDiagnostics" component={RunDiagnostics} />
          <Route component={NoMatch} />
        </Switch>
        </Layout>
      </Router>

    </React.Fragment>
  );
}

export default App;
